____________________________________________________
**IMPORTANT**
THIS LEVEL REQUIRES RVGL 20.0905a (or higher).

This level may be locked depending on championship progress.
Unlock reversed mode by beating time trial challenge time.
____________________________________________________

ELEMENTARY BATTLE

Length: n/a
Length(R):  n/a
Difficulty: n/a

Created: 02/05/2024


____________________________________________________

Description:

The Voltastic Book Fair is here!
Battle between book displays in the multi-purpose room, or sneak through the kitchen to the baseball diamond outside!
A medium arena with clever ramps and tight turns.

____________________________________________________

Patch - 8/25/2024

-Repositioned the sun
-Fixed the dirt material skidmarks/particles
-Fixed reflections in low performance mode

____________________________________________________


Developers:

GJ
-Models
-Textures
-Objects
-Animations
-Lighting and Shading
-Visiboxes

I Spy
-Color Design and Pallette
-Visual Design Advisor
-Animation Advisor
-Sound Recording and Curation


Music is Overdriver_-_Revolt_1999_CentroidS_2021_Remake

Made with Blender Plugin

____________________________________________________

Stats:

World Faces: 4,932
With Instances: 9,248
NCP Faces: 820
Development Time: 5 Months


____________________________________________________

Trivia:

-The multi-purpose room is being used as the book fair, just like real life.
-Can you figure out which car skin is on the cover of RV Stine's GoopClumps book?
-The musical audition poster includes official Re-Volt concept art.
-We always thought the industrial dishwasher at the tray return was an inspirational detail. We had to include it!
-Auditions begin at 3pm sharp! Keep an eye on the time.


____________________________________________________

IMPORTANT
-Items in this directory are built and designed by GJ and I SPY.
-Please consult GJ before altering or reusing assets outside of personal use. (I'm lenient about asset use, don't be afraid to ask)
-Feel free to distribute as is without taking credit from the developers.
-Do not alter this Readme file, and include it in this directory if distributing to friend, family, or whomever. 
-This track directory is free to download and use, it is not for sale and not to be sold. Period.

Re-Volt Hideout
https://forum.re-volt.io/memberlist.php?mode=viewprofile&u=2090

Email (I don't check this too often)
grnthn2@gmail.com

Discord
.?#8017  (replace ? with coffee mug emoji)

I SPY is reclusive, but if all else fails you can try to contact him instead. He also isn't a spy that's just his name.




We hope you thoroughly enjoy this track,
Thank you  :)
