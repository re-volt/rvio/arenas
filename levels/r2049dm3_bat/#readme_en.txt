═══╦═══════╦═══
   ║SUMMARY║
   ╚═══════╝

Name:			R2049 Tundra Battle
Folder Name:		r2049dm3_bat
Author:			Dyspro50
Category:		Extreme Conversion (San Francisco Rush 2049)
Original release date:	December 21, 2015

Tools (from the circuit version):
			MAKEITGOOD
			NGCGeoAPRM (home-maded converter)
			HHD Hex Editor Neo
			Blender 2.73 with Jigebren's Re-Volt Plugin (v2015-02-27)
			rvtmod8
			PRM tool by Kallel
			PaintDotNet


═══╦═════════════════╦═══
   ║TRACK DESCRIPTION║
   ╚═════════════════╝

"Tundra" ("Battle 3" in N64 North-American release) is one of the 8 battle areas found in console versions of San Francisco Rush 2049.

Includes original DC/MAT3 music, "Starsky" by Barry Leitch.


═══╦════════════╦═══
   ║KNOWN ISSUES║
   ╚════════════╝

None so far...


═══╦═══════╦═══
   ║UPDATES║
   ╚═══════╝

2023-07-07 (From the circuit version)
	- Included I/O Pack fixes done by MightyCucumber/SantiTM and others, which include the following:
		- BMP to PNG conversion of textures and track gfx
		- Reduced unnecessary 512px wide track gfx to 256px


═══╦═════╦═══
   ║NOTES║
   ╚═════╝
	
- This track requires RVGL 15.1220a or higher

- Mesh XBLOQ.prm done by Dyspro50. The rest is entirely done by Rush 2049 artists, excluding very minor textures and meshing tweaks.

- If you have any questions, please contact me on The Re-Volt Hideout, or by e-mail at dyspro50@gmail.com


═══╦═════════════════╦═══
   ║SPECIAL THANKS TO║
   ╚═════════════════╝

- Midway and Atari Games for this wonderful game and for the original mesh!

- All Re-Volters who support the project

- All Re-Volters who contributed to the track fixes

- You for downloading this track!



 Dyspro


********************************************************************************************************
