﻿
*** THIS IS THE FRENCH VERSION OF THE README. FOR ENGLISH, PLEASE SEE readme_en.txt. ***

═══╦══════╦═══
   ║RÉSUMÉ║
   ╚══════╝

Nom:				R2049 Tundra Battle
Nom du dossier:			r2049dm3_bat
Auteur:				Dyspro50
Catégorie:			Extreme Conversion (San Francisco Rush 2049)
Date de lancement originale:	21 décembre 2015

Outils (Depuis la version Circuit):
			MAKEITGOOD
			NGCGeoAPRM (convertisseur fait-maison)
			HHD Hex Editor Neo
			Blender 2.73 avec le plug-in de Jigebren pour Re-Volt (v2015-02-27)
			rvtmod8
			Outil PRM par Kallel
			PaintDotNet


═══╦═══════════╦═══
   ║DESCRIPTION║
   ╚═══════════╝

«Tundra» («Battle 3» dans la version N64 nord-américaine) est une des 8 arènes de bataille présentes dans les versions consoles de San Francisco Rush 2049.

Inclut la musique originale des versions DC/MAT3 du jeux, 'Starsky' par Barry Leitch.


═══╦════════════════╦═══
   ║PROBLÈMES CONNUS║
   ╚════════════════╝

Aucun jusqu'à date...


═══╦════════════╦═══
   ║MISES À JOUR║
   ╚════════════╝

2023-07-07 (Depuis la version Circuit)
	- Inclusion des correctifs I/O Pack par MightyCucumber/SantiTM et autres, incluant les correctifs suivants :
		- Conversion des textures du format BMP vers PNG
		- Réduction du gfx du circuit de 512px vers 256px


═══╦═════╦═══
   ║NOTES║
   ╚═════╝

- Ce circuit nécessite RVGL 15.1220a ou ultérieur

- Remerciments à Midway et Atari Games pour ce splendide jeu, ainsi que pour les mailles originales!

- Pour toutes questions, S.V.P. contactez-moi sur The Re-Volt Hideout, ou par courriel à l'adresse dyspro50@gmail.com


═══╦══════════════════╦═══
   ║REMERCIMENTS À/AUX║
   ╚══════════════════╝

- Midway et Atari Games pour ce splendide jeu, ainsi que pour les mailles originales!

- L'ensemble des Re-Volteurs qui ont supporté le projet

- L'ensemble des Re-Volteurs qui ont participé aux correctifs

- Vous pour avoir téléchargé le circuit!



 Dyspro


********************************************************************************************************
