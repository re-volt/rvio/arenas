Content:
>>> Track information
>>> Changelog
>>> Description
>>> Contact
>>> Tools used
>>> Textures used and thanks
>>> Disclaimer

Track information:
>>> Name: Two neighborhoods Battle
>>> Folder name: nhoods_battle
>>> Author: Keyran
>>> Year of first release: 2021
>>> Year of last update: 2022
>>> Category: Battle Tag Arena
>>> Difficulty: Extreme

Changelog:
Version 1.0.1:
+ Added missing objects (Sprinkler and lanterns)

Version 1.1:
+ Removed almost all the cave part, because the arena was too big.
+ Added a new path (will you notice it at your first try?)
+ Added two instances from Toy World tracks in the biggest bedroom
+ Removed some useless files

Version 1.2:
Rodik brought the following updates:
+ Added instances and objects
+ Better shading
+ Removed some parts of Toys in the Hood 2 that were far from the arena
+ Slightly modified world and collision
I also modified a little bit the world and collision files.

Version 1.2.1:
I made the following changes suggested by Rodik:
+ Added check-ins
+ Added a road cone
Also, Rodik took the screenshot I have added on the download page

Version 1.3:
+ The sidewalk close to the start line is now horizontal (another update from Rodik)
+ Fixed a star that was set to Practice star instead of Global weapon
+ Moved two instances so they now touch the sidewalk after it was modified

Version 1.3.1:
+ Fixed a z-fighting issue in a window.
+ Improved shading in the bedroom.

Description:
This is a battle tag arena based on the new neighborhood in Two neighborhoods.

Contact:
>>> Discord: Keyran#3667

Tools used:
>>> Photofiltre 7 for textures
>>> Blender with Marv's plugin
>>> RVGL (Makeitgood)
>>> WorldCut

Textures used and thanks:
Textures a, b, c, d, e, f, g, h, i, j, k, l, p, q and u are stock textures.
Textures m, n, r, s, t, v and x are likely in public domain (I haven't found any restriction on them). I modified texture r.
Texture o has stock textures, and on the left, a texture made by myself from textures m and n.
Texture w is scratch made by me and contains the rvgl logo.
All instances and models are either stock or made by me. Some stock models are slightly modified to take into account the location of their corresponding textures.

Thanks Paperman who changed the colors of most of the new textures and edited vertex painting in the world file, the national roads, the grass and the cliff look better.
Thanks Acclaim / Probe for the original game, and the community that keeps the game alive.
Thanks Rodik for various updates.

Disclaimer:
Don't use any part of this track for commercial purposes (except the textures m, n, r, s, t, v and x).