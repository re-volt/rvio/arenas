﻿═══╦═══════╦═══
   ║SUMMARY║
   ╚═══════╝

Name:			R2049 Plaza Battle
Folder Name:		r2049dm6_bat
Author:			Dyspro50
Category:		Extreme Conversion (San Francisco Rush 2049)
Original release date:	November 27, 2019

Tools (from the circuit version):
			MAKEITGOOD
			NGCGeoAPRM (home-made converter)
			Blender 2.73 with Jigebren's Re-Volt Plugin (v2015-02-27)
			rvtmod8
			rvglue
			PaintDotNet


═══╦═════════════════╦═══
   ║TRACK DESCRIPTION║
   ╚═════════════════╝

"Plaza" ("Battle 6" in N64 North-American release) is one of the 8 battle areas found in console versions of San Francisco Rush 2049. This is the model from DC/MAT3 version and it's part of the "Re-Volt X Rush 2049" long-term project.

Includes original DC/MAT3 music, "Garage" by Barry Leitch.


═══╦════════════╦═══
   ║KNOWN ISSUES║
   ╚════════════╝

Theres a minor bug where the music will make a strange noise 1 of 2 times when it restarts.

═══╦═══════╦═══
   ║UPDATES║
   ╚═══════╝

2023-07-08 (From the circuit version)
	- Included I/O Pack fixes done by MightyCucumber/SantiTM and others, which include the following:
		- Added collision to the upper part of the track
		- Reduced unnecessary 512px wide track gfx to 256px
	- Fixed a shadow box near the section with 4 identical buildings
	- Fixed minor texturing issue in the tunnel inside the tall building


═══╦═════╦═══
   ║NOTES║
   ╚═════╝

- This track requires RVGL 15.1220a or higher

- Meshes DEPLIGNE.prm and XBLOQ.prm done by myself. The rest is entirely done by Rush 2049 artists, with some exceptions.

	- You are allowed to reuse any files only if you credit their respective author(s).

- If you have any questions, please contact me on The Re-Volt Hideout, or by e-mail at dyspro50@gmail.com


═══╦═════════════════╦═══
   ║SPECIAL THANKS TO║
   ╚═════════════════╝

- Midway and Atari Games for this wonderful game and for the original mesh!

- All Re-Volters who support the project

- All Re-Volters who contributed to the track fixes

- You for downloading this track!



 Dyspro


********************************************************************************************************
