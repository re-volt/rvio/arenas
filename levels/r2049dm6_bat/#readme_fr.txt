﻿
*** THIS IS THE FRENCH VERSION OF THE README. FOR ENGLISH, PLEASE SEE #readme_en.txt ***

═══╦══════╦═══
   ║RÉSUMÉ║
   ╚══════╝

Nom:				R2049 Plaza Battle
Nom du dossier:			r2049dm6_bat
Auteur:				Dyspro50
Catégorie:			Extreme Conversion (San Francisco Rush 2049)
Date de lancement originale:	27 novembre 2019

Outils (Depuis la version Circuit):
			MAKEITGOOD
			NGCGeoAPRM (convertisseur fait-maison)
			Blender 2.73 avec le plug-in de Jigebren pour Re-Volt (v2015-02-27)
			rvtmod8			
			rvglue
			PaintDotNet


═══╦═══════════╦═══
   ║DESCRIPTION║
   ╚═══════════╝

«Plaza» («Battle 6» dans la version N64 nord-américaine) est une des 8 arènes de bataille présentes dans les versions consoles de San Francisco Rush 2049. Ce circuit est convertit depuis la version DC/MAT3 et fait partie du projet long-terme «Re-Volt X Rush 2049».

Inclut la musique originale des versions DC/MAT3 du jeux, «Garage» par Barry Leitch.


═══╦════════════════╦═══
   ║PROBLÈMES CONNUS║
   ╚════════════════╝

Il y a un bogue mineur où la musique fait un bruit étrange 1 fois sur 2 lorsqu'elle recommence.


═══╦════════════╦═══
   ║MISES À JOUR║
   ╚════════════╝

2023-07-08 (Depuis la version Circuit)
	- Inclusion des correctifs I/O Pack par MightyCucumber/SantiTM et autres, incluant les correctifs suivants :
		- Ajout de collisions au dessus de l'arène
		- Réduction du gfx du circuit de 512px vers 256px
	- Résolution d'un problème avec une boîte d'ombrage (shadow box) près de la section avec 4 bâtisses identiques
	- Résolution d'un problème mineur de texturage dans le tunnel dans la grande bâtisse


═══╦═════╦═══
   ║NOTES║
   ╚═════╝
- Ce circuit nécessite RVGL 15.1220a ou ultérieur

- Les mailles DEPLIGNE.prm et XBLOQ.prm sont conçues par moi. Tout le reste est conçu par l'équipe de design de Rush 2049, sinon légèrement modifié moi-même.

	- Vous avez l'autorisation de les réutiliser comme vous le voulez, mais seulement si vous mentionnez son auteur original.

- Pour toutes questions, S.V.P. contactez-moi sur The Re-Volt Hideout, ou par courriel à l'adresse dyspro50@gmail.com


═══╦══════════════════╦═══
   ║REMERCIMENTS À/AUX║
   ╚══════════════════╝

- Midway et Atari Games pour ce splendide jeu, ainsi que pour les mailles originales!

- L'ensemble des Re-Volteurs qui ont supporté le projet

- L'ensemble des Re-Volteurs qui ont participé aux correctifs

- Vous pour avoir téléchargé le circuit!



 Dyspro


********************************************************************************************************
