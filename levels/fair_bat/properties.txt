SPARK {
  ID              4                             
  Name            "GRAVEL"                      

  CollideWorld    true                          
  CollideObject   true                          
  CollideCam      true                          
  HasTrail        false                         
  FieldAffect     true                         
  Spins           false                        
  Grows           false                       
  Additive        false                     
  Horizontal      false                      

  Size            6.00000 6.00000            
  UV              0.517921 0.751165           
  UVSize          0.039766 0.039766           
  TexturePage     48                          
  Color           120 118 115                  

  Mass            0.100000                      
  Resistance      0.060000                      
  Friction        0.700000                      
  Restitution     0.200000                      

  LifeTime        1.500000                      
  LifeTimeVar     0.500000                     

  SpinRate        0.000000                     
  SpinRateVar     25.000000                   

  SizeVar         0.000000                    
  GrowRate        0.000000                     
  GrowRateVar     0.000000                     

  TrailType       -1                           
}


SPARK {
  ID              6                             
  Name            "GRASS"                       

  CollideWorld    true                          
  CollideObject   true                          
  CollideCam      false                         
  HasTrail        false                         
  FieldAffect     true                          
  Spins           true                          
  Grows           false                        
  Additive        false                         
  Horizontal      false                         

  Size            8.000000 8.000000             
  UV              0.506299 0.684851             
  UVSize          0.05605 0.05605             
  TexturePage     48                            
  Color           127 127 127                   

  Mass            0.100000                      
  Resistance      0.020000                      
  Friction        1.000000                      
  Restitution     0.500000                      

  LifeTime        1.000000                      
  LifeTimeVar     0.300000                      

  SpinRate        0.000000                      
  SpinRateVar     15.000000                    

  SizeVar         0.000000                      
  GrowRate        0.000000                      
  GrowRateVar     0.000000                     

  TrailType       -1                            
}
