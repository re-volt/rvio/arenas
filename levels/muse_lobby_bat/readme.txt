********************************************************************************
**                               Changelog                                    **
********************************************************************************
--------------------------------------------------------------------------------
Version 23.1010a

- Bugfix release

# Removed doubles (EstebanMz)

--------------------------------------------------------------------------------
Version 20.1229a

- Bugfix release

# Added Pickups for the arena (Zeino)

--------------------------------------------------------------------------------

Version 20.0801a

- Bugfix release

# Fixed possible escape at 1st floor (MightyCucumber)
# Added invisible walls for avoid any escapes (MightyCucumber)
# Support for 16 players (Santiii727)

--------------------------------------------------------------------------------

Version 15.1221a

- First Alpha release (Marv)

--------------------------------------------------------------------------------
Name: Museum Lobby Battle
Release date: 2015
Track Type: Battle Tag (Multiplayer only)
Difficulty: Medium
--------------------------------------------------------------------------------
Note: Here's a proper museum-themed battle arena, made from parts of Museum 1.
There are three positions for the star. One of them is a bit tricky.
Have fun!

Base and original arena is made by Marv.
General Fixes is made by MightyCucumber.
Pickups is made by Marv. and Zeino.
Inf edit is made by Santiii727.

Special Notes:
Thanks to the guys for fix this old short arena and contributing to the re-volt I/O.


©️ 2015-2020 Marv and RVGL I/O Community.