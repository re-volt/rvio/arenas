﻿
*** THIS IS THE FRENCH VERSION OF THE README. FOR ENGLISH, PLEASE SEE #readme_en.txt ***

═══╦══════╦═══
   ║RÉSUMÉ║
   ╚══════╝

Nom:				R2049 Stadium Sattle
Nom du dossier:			r2049dm1_bat
Auteur:				Dyspro50
Catégorie:			Extreme Conversion (San Francisco Rush 2049)
Date de complétion originale:	4 juillet 2023

Outils (Depuis la version Circuit):
			MAKEITGOOD
			NGCGeoAPRM (convertisseur fait-maison)
			Blender 2.73 avec le plug-in de Jigebren pour Re-Volt (v2015-02-27)
			rvtmod8
			rvglue
			PaintDotNet

			
═══╦═══════════╦═══
   ║DESCRIPTION║
   ╚═══════════╝

«Stadium» («Battle 1» dans la version N64 nord-américaine) est une des 8 arènes de bataille 
présentes dans les versions consoles de San Francisco Rush 2049.
Ce circuit est convertit depuis la version DC/MAT3 et fait partie du projet long-terme «Re-Volt X Rush 2049».

Inclut la musique originale des versions DC/MAT3 du jeux, 'High' par Barry Leitch.


═══╦════════════════╦═══
   ║PROBLÈMES CONNUS║
   ╚════════════════╝

Aucun jusqu'à date...


═══╦════════════╦═══
   ║MISES À JOUR║
   ╚════════════╝

2023-07-04 (Depuis la version Circuit)
	- Inclusion des correctifs I/O Pack par MightyCucumber/SantiTM, incluant les correctifs suivants :
		- Résolution d'une collision de la double-rampe sur la partie ouest de l'arène, originallement signalé par NotNoname le 9 avril 2020
		- Ajout de collisions au dessus de l'arène		


═══╦═════╦═══
   ║NOTES║
   ╚═════╝
	
- Ce circuit n'est compatible qu'avec RVGL et il a été principalement conçu avec le build 16.1210.

- Les mailles DEPLIGNE.prm et XBLOQ.prm sont conçues par moi. Tout le reste est conçu par l'équipe de design de Rush 2049, sinon légèrement modifiés.

	- Vous avez l'autorisation de les réutiliser comme vous le voulez, mais seulement si vous mentionnez son auteur original.
	
- Pour toutes questions, S.V.P. contactez-moi sur The Re-Volt Hideout, ou par courriel à l'adresse dyspro50@gmail.com.


═══╦══════════════════╦═══
   ║REMERCIMENTS À/AUX║
   ╚══════════════════╝

- Midway et Atari Games pour ce splendide jeu, ainsi que pour les mailles originales !

- L'ensemble des Re-Volteurs qui ont supporté le projet

- L'ensemble des Re-Volteurs qui ont participé aux correctifs

- Vous pour avoir téléchargé le circuit !



 Dyspro


********************************************************************************************************
