﻿═══╦═══════╦═══
   ║SUMMARY║
   ╚═══════╝

Name:			R2049 Stadium Battle
Folder Name:		r2049dm1_bat
Author:			Dyspro50
Category:		Extreme Conversion (San Francisco Rush 2049)
Original release date:	July 4, 2023

Tools (From the circuit version):
			MAKEITGOOD
			NGCGeoAPRM (home-made converter)
			Blender 2.73 with Jigebren's Re-Volt Plugin (v2015-02-27)
			rvtmod8
			rvglue
			PaintDotNet


═══╦═════════════════╦═══
   ║TRACK DESCRIPTION║
   ╚═════════════════╝

"Stadium" ("Battle 1" in N64 North-American release) is one of the 8 battle areas found in console versions of San Francisco Rush 2049.
This is the model from DC/MAT3 version and it's part of the "Re-Volt X Rush 2049" long-term project.

Includes original DC/MAT3 music, "High" by Barry Leitch.


═══╦════════════╦═══
   ║KNOWN ISSUES║
   ╚════════════╝

None so far...


═══╦═══════╦═══
   ║UPDATES║
   ╚═══════╝

2023-07-04 (From the circuit version)
	- Included I/O Pack fixes done by MightyCucumber/SantiTM, which include the following :
		- Fixed collision of the large double-ramp at the west part of the arena, first submited by NotNoname on April 9, 2020
		- Added collision to the upper part of the track		


═══╦═════╦═══
   ║NOTES║
   ╚═════╝
	
- This track is only compatible with RVGL and was mainly designed with alpha build 16.1230.

- Meshes DEPLIGNE.prm and XBLOQ.prm done by Dyspro50. The rest is entirely done by Rush 2049 artists, excluding very minor textures and meshing tweaks.

	- You are allowed to reuse any files only if you credit their respective author(s).

- If you have any questions, please contact me on The Re-Volt Hideout, or by e-mail at dyspro50@gmail.com


═══╦═════════════════╦═══
   ║SPECIAL THANKS TO║
   ╚═════════════════╝

- Midway and Atari Games for this wonderful game and for the original mesh !

- All Re-Volters who support the project

- All Re-Volters who contributed to the track fixes

- You for downloading this track !



 Dyspro


********************************************************************************************************
